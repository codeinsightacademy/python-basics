'''
while loop 
1 
2
3
4
5
'''

i = 1
while (i<=5): 
    print(i)
    i=i+1
      
'''
lets party party party party party
lets party party party party party
lets party party party party party
lets party party party party party
lets party party party party party
'''
i = 0
while(i<=4):
    print("Lets ", end="")

    j = 0
    while(j<=4):
        print("party ", end="")        
        j=j+1
 
    i=i+1
    print()

# for loop #
x = ["Priyanka", "20", "Mumbai"]
#print(x)

for i in x:
    print(i)

z = "Priyanka"

for i in range(11):
    if (i==0) or (i%2==0):
       pass 
    else:
        print(i)

print("-----------------------------")

for i in range(10):
    if (i%5==0) or (i%3==0):
        continue
    print(i)

print("-----------------------------")

for i in range(10):
    if (i!=0) and (i%5==0):
       break
    else:
        print(i)













