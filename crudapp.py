import pymysql
import csv
from prettytable import PrettyTable
conn = pymysql.connect(host='localhost', user='diituser', password = "%TGBbgt5", db='ecom')
cur = conn.cursor(pymysql.cursors.DictCursor);

def exportData():
    to_csv = showData();

    keys = to_csv[0].keys()
    file_path = 'users.csv';
    with open(file_path, 'w', newline='') as output_file:
        dict_writer = csv.DictWriter(output_file, keys)
        dict_writer.writeheader()
        dict_writer.writerows(to_csv)

    print(f"Data exported in {file_path}");

def updateData():
    user_id = int(input("Enter id: "));
    user_name = input("Enter name: ");
    user_age = int(input("Enter age: "));
    user_city = input("Enter city: ");
    sql = f"UPDATE `users` SET `name` = '{user_name}', `age` = '{user_age}', `city` = '{user_city}', updated_at = NOW() WHERE `users`.`id` = {user_id}";
    cur.execute(sql);
    conn.commit();
    showData();


def insertData():
    user_name = input("Enter name: ");
    user_age = int(input("Enter age: "));
    user_city = input("Enter city: ");
    sql = f"INSERT INTO `users` (`id`, `name`, `age`, `city`, `added_at`, `updated_at`) VALUES (NULL, '{user_name}', '{user_age}', '{user_city}', NOW(), NOW())";
    cur.execute(sql);
    conn.commit();
    showData();


def deleteData():
    user_id = int(input("Enter id to delete record: "));
    sql = f"DELETE FROM users WHERE id = {user_id}";
    cur.execute(sql);
    conn.commit();
    showData();


def showData():
    sql = "SELECT * FROM users";
    cur.execute(sql)
    records = cur.fetchall()

    #print(records); #this will print list

    t = PrettyTable(['id', 'name','age','city'])

    for user in records:
        t.add_row([user['id'], user['name'], user['age'], user['city']]);

    print(t);
    return records;


option = None;

while(option != 0):
    print("\n================\n");
    print("1. Show Data");
    print("2. Detele data");
    print("3. Insert data");
    print("4. Update data");
    print("5. Export data");
    print("0. Exit");

    option = int(input("Choose Option: "));

    if(option == 1):
        showData();


    if(option == 2):
        deleteData();


    if(option == 3):
        insertData();


    if(option == 4):
        updateData();


    if(option == 5):
        exportData();


    if(option == 0):
        print("Thank you for using our software");
