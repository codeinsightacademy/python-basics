import math
# math functions#
a = round(7.666666666)
b = abs(-12.666)

print(a)
print(math.ceil(7.3))
print(math.floor(7.8))

num = 7.3333333333333
print("{:.1f}".format(num))
#print("{:.2f}".format(7.3333333333333))
print(f"{num:.2f}")


#type conversion: converting variable  type into other type eg: from str to int#
num = input("enter a number: ")
print(type(num))
in_num = int(num)
print(type(in_num))

#type casting: declaring a varible with its type#
x = str(5)
y = int("10")
z = float(5)
passed = bool("True")
print(type(z))

