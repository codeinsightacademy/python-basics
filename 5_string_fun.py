
wish = "Hi There"
greet = 'Hello World'
#print(wish)
#print(greet)

print('he said "I like python"')
print("he said he like's python")

print("""
Hello Everyone, 

Welcome to python classes.
Happy learning! 

CIA
""")


# string function #
name = "tom cat"

print(len(name))
print(name.upper())
print(name.lower())
print(name.capitalize())
print(name.title())

print(name.find('om'))
new_name = name.replace('t','m')
print(new_name)

print(name.find('tom'))
print('tom' in name)

